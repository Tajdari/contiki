

#include <stdio.h>
#include "contiki-conf.h"
#include "net/netstack.h"
#include "net/rime/rime.h"
#include "sys/node-id.h"
//#include "net/mac/tsch/tsch.h"
#include "node-id.h"


uint16_t idp = 0;
uint16_t num [2];

const linkaddr_t add1 =  {0x00,0x15,0x8D,0x00,0x00,0x57,0xDE,0x36}; //{ { 2, 0 } }
const linkaddr_t add2 =  {0x00,0x15,0x9D,0x00,0x00,0x56,0xFF,0x36}; //{ { 3, 0 } }

const linkaddr_t coordinator_addr = {0x00,0x15,0x8D,0x00,0x00,0x57,0xFA,0x00} ; // { { 1, 0 } }
const linkaddr_t destination_addr = {0x00,0x15,0x8D,0x00,0x00,0x57,0xFA,0x00} ; // { { 1, 0 } }

/*---------------------------------------------------------------------------*/
PROCESS(unicast_test_process, "Rime Node");
AUTOSTART_PROCESSES(&unicast_test_process);

/*---------------------------------------------------------------------------*/
static void
recv_uc(struct unicast_conn *c, const linkaddr_t *from)
{
	//linkaddr_t * fromm = from;

	//int var;
	//memcpy(&var, packetbuf_dataptr(), sizeof(var));
	//printf("var=%d\n", var);

	char * a ;
	a = packetbuf_dataptr();

	//if(from->u8[0] == add1.u8[0] && from->u8[1] == add1.u8[1] && from->u8[2] == add1.u8[2] && from->u8[3] == add1.u8[3] && from->u8[4] == add1.u8[4] && from->u8[5] == add1.u8[5] && from->u8[6] == add1.u8[6] && from->u8[7] == add1.u8[7])
	if(!strcmp( a,"hi"))
	{
		num[0]++;
	}

	if(!strcmp( a,"hillo"))
	{
		num[0]++;
	}


	//if(from->u8[0] == add2.u8[0] && from->u8[1] == add2.u8[1] && from->u8[2] == add2.u8[2] && from->u8[3] == add2.u8[3] && from->u8[4] == add2.u8[4] && from->u8[5] == add2.u8[5] && from->u8[6] == add2.u8[6] && from->u8[7] == add2.u8[7])
	if(!strcmp( a,"hello"))
	{
		num[1]++;
	}

  printf("%u ",num[0]);
  printf("%u ",num[1]);
  printf("%s",a);
 // printf("%s \n", packetbuf_datalen(), (char *)packetbuf_dataptr());
  //printf("'0x%04x' \n", packetbuf_datalen(), (uint16_t *)packetbuf_dataptr());
}
/*---------------------------------------------------------------------------*/
static void
sent_uc(struct unicast_conn *ptr, int status, int num_tx)
{

}

static const struct unicast_callbacks unicast_callbacks = { recv_uc, sent_uc };
static struct unicast_conn uc;

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(unicast_test_process, ev, data)
{
  PROCESS_BEGIN();



  if(node_id == 64000)//64000
  {
	  tsch_set_coordinator(1);
	  linkaddr_set_node_addr(&destination_addr);
	  num[0]=0;
	  num[1]=0;
	  printf("1done");

  }
  /*

  if(node_id == 63985)//63985
  {
	  linkaddr_set_node_addr(&add1);
	  printf("2done");
	  idp = 0;

  }

  if(node_id == 56886)//56886
  {
	  linkaddr_set_node_addr(&add2);
	  printf("3done");
	  idp = 0;

  }
  */

  NETSTACK_MAC.on();

  unicast_open(&uc, 146, &unicast_callbacks);


//if(tsch_is_associated)
  while(idp<122) {
  //while(1)
 // {
	 // printf("myid: %u\n",node_id);

    static struct etimer et;

    etimer_set(&et, (2*CLOCK_SECOND)/3);

    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));


    if(node_id == 56886)
    packetbuf_copyfrom("hi", 2);

    else
    packetbuf_copyfrom("hello", 5);

   if(!linkaddr_cmp(&destination_addr, &linkaddr_node_addr)) {

      unicast_send(&uc, &destination_addr);
      idp++;

      printf("idp : %u \n",idp);
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
