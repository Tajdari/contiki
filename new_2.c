
#include <stdio.h>
#include "contiki-conf.h"
#include "net/netstack.h"
#include "net/rime/rime.h"
#include "net/mac/tsch/tsch.h"
#include "sys/node-id.h"

/*
	linkaddr_t link ;

# if node_id == 19847
    link = { {0x00,0x12,0x4B,0x00,0x07,0x9B,0x4D,0x87} } ;

# else
# if node_id == 2052
    link = { {0x00,0x12,0x4B,0x00,0x07,0x97,0x08,0x04} } ;
#endif
#endif
*/
static struct infoT {

  uint16_t id ;
  uint16_t light;
  uint16_t temperature;
  uint16_t humidity;

  //rtimer_clock_t timestamp;
};



uint16_t node_id_h =19847 ;  /////??????? change ?
uint16_t node_id_l =2052 ;  /////??????? change ?
uint16_t node_id_t =2052 ;  /////??????? change ?

uint16_t idp = 0;


struct infoT *real_info ;

const linkaddr_t coordinator_addr =    { {0x00,0x12,0x4B,0x00,0x07,0x9B,0x4D,0x87} }; /////??????? change ?
const linkaddr_t destination_addr =    { {0x00,0x12,0x4B,0x00,0x07,0x9B,0x4D,0x87} }; /////??????? change ?

/*---------------------------------------------------------------------------*/
PROCESS(unicast_test_process, "Rime Node");
AUTOSTART_PROCESSES(&unicast_test_process);

/*---------------------------------------------------------------------------*/
static void
recv_uc(struct unicast_conn *c, const linkaddr_t *from)
{

	 struct infoT *msg;
	 msg = packetbuf_dataptr();

	  uint16_t light=0;
	  uint16_t temperature =22;
	  uint16_t humidity = 1;


	  if(msg -> id == node_id_h)
		  humidity = msg -> humidity;

	  if(msg -> id == node_id_l)
	 	  light = msg -> light;

	  if(msg -> id == node_id_t)
	 	  temperature = msg -> temperature;

	  idp = idp + 1;

  printf("App: unicast message received from %u.%u\n",from->u8[0], from->u8[1]);
  printf("App: h:%u l:%u t:%u  idp:%u \n ", humidity , light , temperature , idp );

}
/*---------------------------------------------------------------------------*/
static void
sent_uc(struct unicast_conn *ptr, int status, int num_tx)
{
  printf("App: unicast message sent, status %u, num_tx %u\n",
   status, num_tx);
}

static const struct unicast_callbacks unicast_callbacks = { recv_uc, sent_uc };
static struct unicast_conn uc;

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(unicast_test_process, ev, data)
{
  PROCESS_BEGIN();

  tsch_set_coordinator(linkaddr_cmp(&coordinator_addr, &linkaddr_node_addr));
  NETSTACK_MAC.on();


  //nodeidsetting_init();


  unicast_open(&uc, 146, &unicast_callbacks);

  while(1) {
    static struct etimer et;

	struct infoT msge;



  //  linkaddr_set_node_addr(&link);

/*
	if(node_id == node_id_l)
	 {msge.light = 2 ; msge.temperature =2; msge -> humidity =  2; printf("2\n");} /////??????? change ?

	//if(node_id == node_id_h)
	 //{msge -> light =2; msge -> temperature = 2;msge -> humidity =  1;printf("1\n");} /////??????? change ?

	if(node_id == node_id_t)
	 {msge.light = 3; msge.temperature =3; printf("%u\n",msge.temperature);} /////??????? change ?

*/
	etimer_set(&et, CLOCK_SECOND);

	PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

	//if(node_id == node_id_t)
	//{
	 msge.id = node_id ;msge.light =  1234 ; msge.temperature =node_id;  msge.humidity = 1;
	 printf("--%u\n",node_id);
	 printf("--%u\n",msge.temperature);
	//}
	// printf("APP:sendig msg  l:%u H:%u t:%u id:%u \n" ,msge.light , msge.temperature ,msge.humidity,msge.id);


 //   etimer_set(&et, CLOCK_SECOND);

 //   printf("APP:sendig msg  l:%u H:%u t:%u id:%u \n" ,msge.light , msge.temperature ,msge.humidity,msge.id);


    printf("------%u\n",&linkaddr_node_addr);
    printf("------%u\n",&linkaddr_node_addr);
 //   PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
    
  //  printf("APP:sendig msg  l:%u H:%u t:%u id:%u \n" ,msge.light , msge.temperature ,msge.humidity,msge.id);

    packetbuf_copyfrom(&msge , sizeof(msge));

   // packetbuf_copyfrom("Hello", 5);

  //  printf("------%u\n",&node_id);


    if(!linkaddr_cmp(&destination_addr, &linkaddr_node_addr)) {
      printf("-----App: sending unicast message from %u to %u.%u\n",node_id, destination_addr.u8[0], destination_addr.u8[1]);
      printf("-----APP:sendig msg id:%u l:%u H:%u t:%u  \n\n" ,msge.id,msge.light , msge.temperature ,msge.humidity);
      unicast_send(&uc, &destination_addr);
     // broadcast_send(&broadcast);
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
